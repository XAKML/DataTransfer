﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.DataTransfer.DbEntities.SaleOrder
{
    /// <summary>
    /// 
    /// </summary>
	public class SoHeaderEntity
    {
        #region Constructors

        public SoHeaderEntity()
        { }

        #endregion

        #region Properties        
        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 8
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 50
        /// </summary>
		public string OrderNo { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string OrderNoExt { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 50
        /// </summary>
		public string OrderType { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 8
        /// </summary>
		public long CustomerId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string CustomerCode { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 50
        /// </summary>
		public string OrderStatus { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 50
        /// </summary>
		public string PayStatus { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 50
        /// </summary>
		public string LogisticsStatus { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string PayType { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 50
        /// </summary>
		public string PayMeth { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 9
        /// </summary>
		public decimal PaidAmount { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 9
        /// </summary>
		public decimal TotalAmount { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 9
        /// </summary>
		public decimal RefundAmount { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 9
        /// </summary>
		public decimal SubTotal { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 9
        /// </summary>
		public decimal ShipFee { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 9
        /// </summary>
		public decimal ShipFeeDiscount { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 9
        /// </summary>
		public decimal CouponDiscount { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 9
        /// </summary>
		public decimal PromotionDiscount { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 50
        /// </summary>
		public string Channel { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 8
        /// </summary>
		public DateTime OrderDate { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string Province { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string City { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string CityZone { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string Zipcode { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 100
        /// </summary>
		public string AddressDetail { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string Consignee { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string PhoneNo { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string MobileNo { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 1000
        /// </summary>
		public string Remark { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<DateTime> DeliveryLimit { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 4
        /// </summary>
		public int IsSplitShipping { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 8
        /// </summary>
		public DateTime ModifiedTime { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 50
        /// </summary>
		public string ModifiedBy { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 8
        /// </summary>
		public DateTime CreatedTime { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 50
        /// </summary>
		public string CreatedBy { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<long> CarrierId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string InvoiceStatus { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<long> OrignalSoId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string OrignalSoNo { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<DateTime> ConfirmTime { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<DateTime> PrintTime { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<long> SiteId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<long> SalesManId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string SalesManName { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string SalesType { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string SalesRelatedSoNo { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 200
        /// </summary>
		public string CollaborateCompany { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<long> CollaborateCompanyId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<long> SupplierId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string OrderCategory { get; set; }


        //public virtual ICollection<ShipmentOrder.ShipmentOrderEntity> ShipOrders { get; set; }
        #endregion

        #region override Mehtods

        public override string ToString()
        {
            return base.ToString();
        }

        #endregion
    }
}
