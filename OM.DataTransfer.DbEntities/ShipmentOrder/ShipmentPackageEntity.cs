﻿using System;

namespace OM.DataTransfer.DbEntities.ShipmentOrder
{
    /// <summary>
    /// 
    /// </summary>
    public class ShipmentPackageEntity
    {
        #region Constructors

        public ShipmentPackageEntity()
        { }

        #endregion

        #region Properties        
        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 8
        /// </summary>
        public long ShipmentOrderId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 50
        /// </summary>
		public string TrackingNo { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 9
        /// </summary>
		public Nullable<decimal> Weight { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<DateTime> ModifiedTime { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string ModifiedBy { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<DateTime> CreatedTime { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string CreatedBy { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string Status { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 4
        /// </summary>
		public int SerialNo { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 8
        /// </summary>
		public long DeliveryListId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string MainTrackingNo { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string SfOriginCode { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string SfDestCode { get; set; }

        //public ShipmentOrder.ShipmentOrderEntity ShipOrder { get; set; }

        #endregion

        #region override Mehtods

        public override string ToString()
        {
            return base.ToString();
        }

        #endregion
    }
}
