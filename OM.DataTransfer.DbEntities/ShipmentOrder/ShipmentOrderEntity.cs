﻿using System;

namespace OM.DataTransfer.DbEntities.ShipmentOrder
{
    /// <summary>
    /// 
    /// </summary>
	public class ShipmentOrderEntity
    {
        #region Constructors

        public ShipmentOrderEntity()
        { }

        #endregion

        #region Properties        
        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 8
        /// </summary>
        public long Id { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<long> SoHeaderId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<DateTime> ShipDate { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string Status { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<long> CarrierId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string CarrierName { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<DateTime> DeliveryLimit { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 9
        /// </summary>
		public Nullable<decimal> ArAmount { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string PayMeth { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<long> WarehouseId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string WarehouseName { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 4
        /// </summary>
		public Nullable<int> GoodsCount { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 4
        /// </summary>
		public Nullable<int> PackageCount { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 4
        /// </summary>
		public Nullable<int> ScannedCount { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<DateTime> ModifiedTime { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string ModifiedBy { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<DateTime> CreatedTime { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string CreatedBy { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 200
        /// </summary>
		public string SiteName { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 8
        /// </summary>
		public Nullable<long> SiteId { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string ShipOrderNo { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 100
        /// </summary>
		public string ActualCarrierName { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 9
        /// </summary>
		public Nullable<decimal> ReceivableAmount { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 4
        /// </summary>
		public int IsInPickbin { get; set; }

        /// <summary>
        /// 
        /// AllowNull: False
        /// Length: 4
        /// </summary>
		public int PickBox { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 50
        /// </summary>
		public string FinPayStatus { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 9
        /// </summary>
		public Nullable<decimal> ArAmountNogift { get; set; }

        /// <summary>
        /// 
        /// AllowNull: True
        /// Length: 4
        /// </summary>
		public Nullable<int> PickSkuNums { get; set; }

        public SaleOrder.SoHeaderEntity Soheader { get; set; }

        /// <summary>
        /// 发货单包裹信息
        /// </summary>
        //public virtual ICollection<ShipmentOrder.ShipmentPackageEntity> Packages { get; set; }
        #endregion

        #region override Mehtods

        public override string ToString()
        {
            return base.ToString();
        }

        #endregion
    }
}
