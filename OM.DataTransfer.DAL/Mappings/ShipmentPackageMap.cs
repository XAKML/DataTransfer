﻿using OM.DataTransfer.DbEntities.ShipmentOrder;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OM.DataTransfer.DAL.Mappings
{
    /// <summary>
    /// 
    /// </summary>
	public class ShipmentPackageMap : EntityTypeConfiguration<ShipmentPackageEntity>
    {
        #region Constructors

        public ShipmentPackageMap()
        {
            this.HasKey(t => new {t.ShipmentOrderId,t.TrackingNo});

            this.HasKey(t => t.TrackingNo);

            this.Property(t => t.ModifiedBy).HasMaxLength(50);

            this.Property(t => t.CreatedBy).HasMaxLength(50);

            this.Property(t => t.Status).HasMaxLength(50);

            this.Property(t => t.SerialNo).IsRequired();

            this.Property(t => t.DeliveryListId).IsRequired();

            this.Property(t => t.MainTrackingNo).HasMaxLength(50);

            this.Property(t => t.SfOriginCode).HasMaxLength(50);

            this.Property(t => t.SfDestCode).HasMaxLength(50);

            this.ToTable("shipment_package");
            this.Property(t => t.ShipmentOrderId).HasColumnName("shipment_order_id"); //shipment_order_id
            this.Property(t => t.TrackingNo).HasColumnName("tracking_no"); //tracking_no
            this.Property(t => t.Weight).HasColumnName("weight"); //weight
            this.Property(t => t.ModifiedTime).HasColumnName("modified_time"); //modified_time
            this.Property(t => t.ModifiedBy).HasColumnName("modified_by"); //modified_by
            this.Property(t => t.CreatedTime).HasColumnName("created_time"); //created_time
            this.Property(t => t.CreatedBy).HasColumnName("created_by"); //created_by
            this.Property(t => t.Status).HasColumnName("status"); //status
            this.Property(t => t.SerialNo).HasColumnName("serial_no"); //serial_no
            this.Property(t => t.DeliveryListId).HasColumnName("delivery_list_id"); //delivery_list_id
            this.Property(t => t.MainTrackingNo).HasColumnName("main_tracking_no"); //main_tracking_no
            this.Property(t => t.SfOriginCode).HasColumnName("sf_origin_code"); //sf_origin_code
            this.Property(t => t.SfDestCode).HasColumnName("sf_dest_code"); //sf_dest_code
            //this.HasRequired(t => t.ShipOrder).WithMany(ship => ship.Packages).HasForeignKey(obj => obj.ShipmentOrderId);
        }

        #endregion

        #region override Mehtods

        public override string ToString()
        {
            return base.ToString();
        }

        #endregion

    }
}
