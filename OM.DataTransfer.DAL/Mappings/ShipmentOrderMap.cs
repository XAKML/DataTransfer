﻿using OM.DataTransfer.DbEntities.ShipmentOrder;
using System.ComponentModel.DataAnnotations.Schema; //程序集 EntityFramework.dll, v6.0.0.0
using System.Data.Entity.ModelConfiguration;//程序集 EntityFramework.dll, v6.0.0.0

namespace OM.DataTransfer.DAL.Mappings
{
    /// <summary>
    /// 
    /// </summary>
	public class ShipmentOrderMap : EntityTypeConfiguration<ShipmentOrderEntity>
    {
        #region Constructors

        public ShipmentOrderMap()
        {
            this.HasKey(t => t.Id);

            this.Property(t => t.Id)
                        .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            //this.Property(t => t.SoHeaderId).IsRequired();

            this.Property(t => t.Status).HasMaxLength(50);

            this.Property(t => t.CarrierName).HasMaxLength(50);

            this.Property(t => t.PayMeth).HasMaxLength(50);

            this.Property(t => t.WarehouseName).HasMaxLength(50);

            this.Property(t => t.ModifiedBy).HasMaxLength(50);

            this.Property(t => t.CreatedBy).HasMaxLength(50);

            this.Property(t => t.SiteName).HasMaxLength(200);

            this.Property(t => t.ShipOrderNo).HasMaxLength(50);

            this.Property(t => t.ActualCarrierName).HasMaxLength(100);

            this.Property(t => t.IsInPickbin).IsRequired();

            this.Property(t => t.PickBox).IsRequired();

            this.Property(t => t.FinPayStatus).HasMaxLength(50);

            this.ToTable("shipment_order");
            this.Property(t => t.Id).HasColumnName("id"); //id
            this.Property(t => t.SoHeaderId).HasColumnName("so_header_id"); //so_header_id
            this.Property(t => t.ShipDate).HasColumnName("ship_date"); //ship_date
            this.Property(t => t.Status).HasColumnName("status"); //status
            this.Property(t => t.CarrierId).HasColumnName("carrier_id"); //carrier_id
            this.Property(t => t.CarrierName).HasColumnName("carrier_name"); //carrier_name
            this.Property(t => t.DeliveryLimit).HasColumnName("delivery_limit"); //delivery_limit
            this.Property(t => t.ArAmount).HasColumnName("ar_amount"); //ar_amount
            this.Property(t => t.PayMeth).HasColumnName("pay_meth"); //pay_meth
            this.Property(t => t.WarehouseId).HasColumnName("warehouse_id"); //warehouse_id
            this.Property(t => t.WarehouseName).HasColumnName("warehouse_name"); //warehouse_name
            this.Property(t => t.GoodsCount).HasColumnName("goods_count"); //goods_count
            this.Property(t => t.PackageCount).HasColumnName("package_count"); //package_count
            this.Property(t => t.ScannedCount).HasColumnName("scanned_count"); //scanned_count
            this.Property(t => t.ModifiedTime).HasColumnName("modified_time"); //modified_time
            this.Property(t => t.ModifiedBy).HasColumnName("modified_by"); //modified_by
            this.Property(t => t.CreatedTime).HasColumnName("created_time"); //created_time
            this.Property(t => t.CreatedBy).HasColumnName("created_by"); //created_by
            this.Property(t => t.SiteName).HasColumnName("site_name"); //site_name
            this.Property(t => t.SiteId).HasColumnName("site_id"); //site_id
            this.Property(t => t.ShipOrderNo).HasColumnName("ship_order_no"); //ship_order_no
            this.Property(t => t.ActualCarrierName).HasColumnName("actual_carrier_name"); //actual_carrier_name
            this.Property(t => t.ReceivableAmount).HasColumnName("receivable_amount"); //receivable_amount
            this.Property(t => t.IsInPickbin).HasColumnName("is_in_pickbin"); //is_in_pickbin
            this.Property(t => t.PickBox).HasColumnName("pick_box"); //pick_box
            this.Property(t => t.FinPayStatus).HasColumnName("fin_pay_status"); //fin_pay_status
            this.Property(t => t.ArAmountNogift).HasColumnName("ar_amount_noGift"); //ar_amount_noGift
            this.Property(t => t.PickSkuNums).HasColumnName("pick_sku_nums"); //pick_sku_nums
            //this.HasRequired(obj => obj.Soheader).WithMany(o=>o.ShipOrders).HasForeignKey(t=>t.SoHeaderId).WillCascadeOnDelete(true);
        }

        #endregion

        #region override Mehtods

        public override string ToString()
        {
            return base.ToString();
        }

        #endregion

    }
}
