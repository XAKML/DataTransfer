﻿using OM.DataTransfer.DbEntities.SaleOrder;
using System.ComponentModel.DataAnnotations.Schema; //程序集 EntityFramework.dll, v6.0.0.0
using System.Data.Entity.ModelConfiguration;//程序集 EntityFramework.dll, v6.0.0.0

namespace OM.DataTransfer.DAL.Mappings
{
    /// <summary>
    /// 
    /// </summary>
    public class SoHeaderMap : EntityTypeConfiguration<SoHeaderEntity>
    {
        #region Constructors

        public SoHeaderMap()
        {
            this.HasKey(t => t.Id);

            this.Property(t => t.Id)
                        .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            this.Property(t => t.OrderNo).IsRequired().HasMaxLength(50);

            this.Property(t => t.OrderNoExt).HasMaxLength(50);

            this.Property(t => t.OrderType).IsRequired().HasMaxLength(50);

            this.Property(t => t.CustomerId).IsRequired();

            this.Property(t => t.CustomerCode).HasMaxLength(50);

            this.Property(t => t.OrderStatus).IsRequired().HasMaxLength(50);

            this.Property(t => t.PayStatus).IsRequired().HasMaxLength(50);

            this.Property(t => t.LogisticsStatus).IsRequired().HasMaxLength(50);

            this.Property(t => t.PayType).HasMaxLength(50);

            this.Property(t => t.PayMeth).IsRequired().HasMaxLength(50);

            this.Property(t => t.PaidAmount).IsRequired();

            this.Property(t => t.TotalAmount).IsRequired();

            this.Property(t => t.RefundAmount).IsRequired();

            this.Property(t => t.SubTotal).IsRequired();

            this.Property(t => t.ShipFee).IsRequired();

            this.Property(t => t.ShipFeeDiscount).IsRequired();

            this.Property(t => t.CouponDiscount).IsRequired();

            this.Property(t => t.PromotionDiscount).IsRequired();

            this.Property(t => t.Channel).IsRequired().HasMaxLength(50);

            this.Property(t => t.OrderDate).IsRequired();

            this.Property(t => t.Province).HasMaxLength(50);

            this.Property(t => t.City).HasMaxLength(50);

            this.Property(t => t.CityZone).HasMaxLength(50);

            this.Property(t => t.Zipcode).HasMaxLength(50);

            this.Property(t => t.AddressDetail).HasMaxLength(100);

            this.Property(t => t.Consignee).HasMaxLength(50);

            this.Property(t => t.PhoneNo).HasMaxLength(50);

            this.Property(t => t.MobileNo).HasMaxLength(50);

            this.Property(t => t.Remark).HasMaxLength(1000);

            this.Property(t => t.IsSplitShipping).IsRequired();

            this.Property(t => t.ModifiedTime).IsRequired();

            this.Property(t => t.ModifiedBy).IsRequired().HasMaxLength(50);

            this.Property(t => t.CreatedTime).IsRequired();

            this.Property(t => t.CreatedBy).IsRequired().HasMaxLength(50);

            this.Property(t => t.InvoiceStatus).HasMaxLength(50);

            this.Property(t => t.OrignalSoNo).HasMaxLength(50);

            this.Property(t => t.SalesManName).HasMaxLength(50);

            this.Property(t => t.SalesType).HasMaxLength(50);

            this.Property(t => t.SalesRelatedSoNo).HasMaxLength(50);

            this.Property(t => t.CollaborateCompany).HasMaxLength(200);

            this.Property(t => t.OrderCategory).HasMaxLength(50);

            this.ToTable("so_header");
            this.Property(t => t.Id).HasColumnName("id"); //id
            this.Property(t => t.OrderNo).HasColumnName("order_no"); //order_no
            this.Property(t => t.OrderNoExt).HasColumnName("order_no_ext"); //order_no_ext
            this.Property(t => t.OrderType).HasColumnName("order_type"); //order_type
            this.Property(t => t.CustomerId).HasColumnName("customer_id"); //customer_id
            this.Property(t => t.CustomerCode).HasColumnName("customer_code"); //customer_code
            this.Property(t => t.OrderStatus).HasColumnName("order_status"); //order_status
            this.Property(t => t.PayStatus).HasColumnName("pay_status"); //pay_status
            this.Property(t => t.LogisticsStatus).HasColumnName("logistics_status"); //logistics_status
            this.Property(t => t.PayType).HasColumnName("pay_type"); //pay_type
            this.Property(t => t.PayMeth).HasColumnName("pay_meth"); //pay_meth
            this.Property(t => t.PaidAmount).HasColumnName("paid_amount"); //paid_amount
            this.Property(t => t.TotalAmount).HasColumnName("total_amount"); //total_amount
            this.Property(t => t.RefundAmount).HasColumnName("refund_amount"); //refund_amount
            this.Property(t => t.SubTotal).HasColumnName("sub_total"); //sub_total
            this.Property(t => t.ShipFee).HasColumnName("ship_fee"); //ship_fee
            this.Property(t => t.ShipFeeDiscount).HasColumnName("ship_fee_discount"); //ship_fee_discount
            this.Property(t => t.CouponDiscount).HasColumnName("coupon_discount"); //coupon_discount
            this.Property(t => t.PromotionDiscount).HasColumnName("promotion_discount"); //promotion_discount
            this.Property(t => t.Channel).HasColumnName("channel"); //channel
            this.Property(t => t.OrderDate).HasColumnName("order_date"); //order_date
            this.Property(t => t.Province).HasColumnName("province"); //province
            this.Property(t => t.City).HasColumnName("city"); //city
            this.Property(t => t.CityZone).HasColumnName("city_zone"); //city_zone
            this.Property(t => t.Zipcode).HasColumnName("zipcode"); //zipcode
            this.Property(t => t.AddressDetail).HasColumnName("address_detail"); //address_detail
            this.Property(t => t.Consignee).HasColumnName("consignee"); //consignee
            this.Property(t => t.PhoneNo).HasColumnName("phone_no"); //phone_no
            this.Property(t => t.MobileNo).HasColumnName("mobile_no"); //mobile_no
            this.Property(t => t.Remark).HasColumnName("remark"); //remark
            this.Property(t => t.DeliveryLimit).HasColumnName("delivery_limit"); //delivery_limit
            this.Property(t => t.IsSplitShipping).HasColumnName("is_split_shipping"); //is_split_shipping
            this.Property(t => t.ModifiedTime).HasColumnName("modified_time"); //modified_time
            this.Property(t => t.ModifiedBy).HasColumnName("modified_by"); //modified_by
            this.Property(t => t.CreatedTime).HasColumnName("created_time"); //created_time
            this.Property(t => t.CreatedBy).HasColumnName("created_by"); //created_by
            this.Property(t => t.CarrierId).HasColumnName("carrier_id"); //carrier_id
            this.Property(t => t.InvoiceStatus).HasColumnName("invoice_status"); //invoice_status
            this.Property(t => t.OrignalSoId).HasColumnName("orignal_so_id"); //orignal_so_id
            this.Property(t => t.OrignalSoNo).HasColumnName("orignal_so_no"); //orignal_so_no
            this.Property(t => t.ConfirmTime).HasColumnName("confirm_time"); //confirm_time
            this.Property(t => t.PrintTime).HasColumnName("print_time"); //print_time
            this.Property(t => t.SiteId).HasColumnName("site_id"); //site_id
            this.Property(t => t.SalesManId).HasColumnName("sales_man_id"); //sales_man_id
            this.Property(t => t.SalesManName).HasColumnName("sales_man_name"); //sales_man_name
            this.Property(t => t.SalesType).HasColumnName("sales_type"); //sales_type
            this.Property(t => t.SalesRelatedSoNo).HasColumnName("sales_related_so_no"); //sales_related_so_no
            this.Property(t => t.CollaborateCompany).HasColumnName("collaborate_company"); //collaborate_company
            this.Property(t => t.CollaborateCompanyId).HasColumnName("collaborate_company_id"); //collaborate_company_id
            this.Property(t => t.SupplierId).HasColumnName("supplier_id"); //supplier_id
            this.Property(t => t.OrderCategory).HasColumnName("order_category"); //order_category
            //this.HasMany(t => t.ShipOrders).WithRequired(ship => ship.Soheader).Map(o=>o.MapKey("so_header_id"));
        }

        #endregion

        #region override Mehtods

        public override string ToString()
        {
            return base.ToString();
        }

        #endregion

    }
}
