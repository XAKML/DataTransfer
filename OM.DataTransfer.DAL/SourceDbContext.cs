﻿using System.Data.Entity;

namespace OM.DataTransfer.DAL
{
    public class SourceDbContext:DbContext
    {
        #region Constructors
        static SourceDbContext()
        {
            Database.SetInitializer<SourceDbContext>(null);
        }
        /// <summary>
        /// 使用默认的数据库连接
        /// </summary>
        public SourceDbContext(string connectionString)
            : base(connectionString)
        {
        }

        /// <summary>
        /// 使用指定的数据库连接
        /// </summary>
        /// <param name="connection"></param>
        public SourceDbContext(System.Data.Common.DbConnection connection)
            : base(connection, true)
        {
        }
        #endregion
        public DbSet<DbEntities.SaleOrder.SoHeaderEntity> SoHeaderSet { get; set; }

        public DbSet<DbEntities.ShipmentOrder.ShipmentOrderEntity> ShipmentOrderSet { get; set; }

        public DbSet<DbEntities.ShipmentOrder.ShipmentPackageEntity> ShipPackageSet { get; set; }

        public DbSet<DbEntities.ShipmentOrder.DeliverySiteEntity> DeliverySiteSet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new Mappings.SoHeaderMap());
            modelBuilder.Configurations.Add(new Mappings.ShipmentOrderMap());
            modelBuilder.Configurations.Add(new Mappings.ShipmentPackageMap());
            modelBuilder.Configurations.Add(new Mappings.DeliverySiteMap());
        }
    }
}
