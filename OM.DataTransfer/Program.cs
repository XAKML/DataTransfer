﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OM.DataTransfer.DbEntities;
using OM.DataTransfer.DbEntities.SaleOrder;

namespace OM.DataTransfer
{
    /****
     * 
     * OM数据导入导出服务
     * 
     * **/
    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            List<OM.DataTransfer.DbEntities.ShipmentOrder.DeliverySiteEntity> site_list = null;
            using (OM.DataTransfer.DAL.TargetDbContext sourceDB = new DAL.TargetDbContext(ConfigurationManager.ConnectionStrings["target_db"].ConnectionString))
            {
                #region  获取原始数据
                //sourceDB.Database.Log = Console.WriteLine;
                site_list = sourceDB.DeliverySiteSet.OrderByDescending(obj=>obj.CreateTime).Take(1).ToList();
                foreach (var order in site_list)
                {
                    Console.Write(order.Name);
                }
                #endregion
            }

            using (OM.DataTransfer.DAL.SourceDbContext target = new DAL.SourceDbContext(ConfigurationManager.ConnectionStrings["source_db"].ConnectionString))
            {
                #region  获取原始数据
                //sourceDB.Database.Log = Console.WriteLine;
                target.DeliverySiteSet.Add(site_list[0]);
                int affectRows = target.SaveChanges();
                Console.WriteLine(affectRows);
                #endregion
            }
            Console.Write("press any key to continue...");
            Console.ReadKey();
        }

        void SaleOrderTransfer()
        {
            List<OM.DataTransfer.DbEntities.SaleOrder.SoHeaderEntity> order_list = null;
            using (OM.DataTransfer.DAL.SourceDbContext sourceDB = new DAL.SourceDbContext(ConfigurationManager.ConnectionStrings["source_db"].ConnectionString))
            {
                #region  获取原始数据
                //sourceDB.Database.Log = Console.WriteLine;
                order_list = sourceDB.SoHeaderSet.Where(obj => obj.OrderStatus == "已发货").OrderByDescending(obj => obj.CreatedTime).Take(1).ToList();
                foreach (var order in order_list)
                {
                    Console.Write(order.OrderNo);
                    if (null != order)
                    {
                        //if (order.ShipOrders != null)
                        //{
                        //    foreach (var ship_order in order.ShipOrders)
                        //    {
                        //        Console.Write("\t" + ship_order.ShipOrderNo);
                        //        if (ship_order.Packages != null)
                        //        {
                        //            foreach (var pkg in ship_order.Packages)
                        //            {
                        //                Console.WriteLine(pkg.TrackingNo);
                        //            }
                        //        }
                        //    }
                        //}
                    }
                    Console.WriteLine();
                }
                #endregion
            }
            using (OM.DataTransfer.DAL.TargetDbContext targetDb = new DAL.TargetDbContext(ConfigurationManager.ConnectionStrings["target_db"].ConnectionString))
            {
                targetDb.Database.Log = Console.WriteLine;
                targetDb.Set<SoHeaderEntity>().Add(order_list[0]);
                targetDb.SaveChanges();
            }
        }
    }
}
