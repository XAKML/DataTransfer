﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;

namespace OM.DataTransfer.DAL.Test
{
    [TestClass]
    public class DemoTest
    {
        /// <summary>
        /// 测试传输DeliverySite对象
        /// </summary>
        [TestMethod]
        public void TestInsertDeliverySite()
        {
            List<OM.DataTransfer.DbEntities.ShipmentOrder.DeliverySiteEntity> site_list = null;
            using (OM.DataTransfer.DAL.TargetDbContext sourceDB = new DAL.TargetDbContext(ConfigurationManager.ConnectionStrings["target_db"].ConnectionString))
            {
                #region  获取原始数据
                //sourceDB.Database.Log = Console.WriteLine;
                site_list = sourceDB.DeliverySiteSet.OrderByDescending(obj => obj.CreateTime).Take(1).ToList();
                foreach (var order in site_list)
                {
                    Console.Write(order.Name);
                }
                #endregion
            }

            using (OM.DataTransfer.DAL.SourceDbContext target = new DAL.SourceDbContext(ConfigurationManager.ConnectionStrings["source_db"].ConnectionString))
            {
                #region  获取原始数据
                //sourceDB.Database.Log = Console.WriteLine;
                target.DeliverySiteSet.Add(site_list[0]);
                int affectRows = target.SaveChanges();
                Console.WriteLine(affectRows);
                #endregion
            }
        }

        /// <summary>
        /// 测试查询指定id的订单信息
        /// </summary>
        [TestMethod,TestCategory("检索订单")]
        public void TestGetSoheaders()
        {
            OM.DataTransfer.DAL.SaleOrder.DbSoHeader dbHeader = new SaleOrder.DbSoHeader();
            var list = dbHeader.GetListByIds(new long[] { 655983L,655981L},System.Configuration.ConfigurationManager.ConnectionStrings["source_db"].ConnectionString);
            if(list != null && list.Count > 0)
            {
                foreach (var item in list)
                {
                    Console.WriteLine(item.OrderNo);
                }   
            }else
            {
                Console.WriteLine("list is null or empty");
            }
        }

        [TestMethod,TestCategory("保存订单")]
        public void TestInsertOrderToTargetDb()
        {
            OM.DataTransfer.DAL.SaleOrder.DbSoHeader dbHeader = new SaleOrder.DbSoHeader();
            var list = dbHeader.GetListByIds(new long[] { 655983L, 655981L }, System.Configuration.ConfigurationManager.ConnectionStrings["source_db"].ConnectionString);
            list.ForEach(obj => {
                obj.Id = 0;
                obj.CreatedTime = DateTime.Now;
                obj.ModifiedTime = DateTime.Now;
                obj.ModifiedBy = "lhx";
                obj.CreatedBy = "om.Transfer";
                obj.SupplierId = obj.SupplierId.HasValue ? obj.SupplierId.Value : 0;
            });
            int inserted_count = dbHeader.BatchInsert(list,System.Configuration.ConfigurationManager.ConnectionStrings["target_db"].ConnectionString);
            Console.WriteLine(inserted_count);
            list.ForEach(obj => Console.WriteLine(obj.Id));
        }
    }
}
